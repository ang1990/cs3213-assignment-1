import java.util.Iterator;
import java.util.List;

public class Output {
	
	public void display(List<String> list) {
	      Iterator<String> titles = list.listIterator(0);
	      
	      while (titles.hasNext()) {
	         System.out.println(titles.next());
	      }
	}
	
}
