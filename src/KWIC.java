import java.util.Scanner;

public class KWIC {

	public static void main(String[] args) {
		
		Input eventI = new Input();
		
		boolean ignoredWords;
		boolean titles;
		
		Scanner in = new Scanner(System.in);
		 
		do {
			System.out.println("Please enter a word to ignore: ");
			String s = in.nextLine();
			System.out.println("You entered: " + s);
			
			eventI.addIgnoredWords(s.toLowerCase());
			
			System.out.println("Do you still want to ignore any more words? (Y/N)");
			String b = in.nextLine();
			
			if(b.contains("Y") || b.contains("y")) {
				ignoredWords = true;
			}
			else {
				ignoredWords = false;
			}
		} while (ignoredWords);

		do {
			System.out.println("Please enter a title: ");
			String s = in.nextLine();
			System.out.println("You entered: " + s);
			
			eventI.addTitle(s.toLowerCase());
			
			System.out.println("Do you still want to enter more titles? (Y/N)");
			String b = in.nextLine();
			
			if(b.contains("Y") || b.contains("y")) {
				titles = true;
			}
			else {
				titles = false;
			}
		} while (titles);
		
	}

}
