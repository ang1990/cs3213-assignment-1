import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Alphabetizer {

	Output eventO = new Output();
	
	private List<String> sortedTitles = new ArrayList<String>();
	
	public void reset() {
		sortedTitles.clear();
	}
	
	public void addNewTitle(CircularShift eventC) {
		sortedTitles.add(eventC.getLatestTitle());
	}
	
	public void sortTitles() {
		Collections.sort(sortedTitles, new Comparator<String>() {
	        @Override
	        public int compare(String o1, String o2) {
	            return Collator.getInstance().compare(o1, o2);
	        }
	    });
		
		eventO.display(sortedTitles);
	}
	
}
