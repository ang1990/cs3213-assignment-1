import java.util.HashSet;
import java.util.Set;
import java.util.LinkedList;

public class Input {
	
	CircularShift eventC = new CircularShift();
	
	private Set<String> ignoredWords = new HashSet<String>();
	private LinkedList<String> titles = new LinkedList<String>();
	
	public void resetEverything() {
		ignoredWords.clear();
		resetTitle();
	}
	
	public void resetTitle() {
		titles.clear();
		eventC.reset();
	}
	
	public boolean isIgnoredWord(String word) {
		return ignoredWords.contains(word);
	}
	
	public void addIgnoredWords(String word) {
		if(!ignoredWords.contains(word)) {
			ignoredWords.add(word);
		}

	}
	
	public void addTitle(String title) {
		if(!titles.contains(title)) {
			titles.add(title);
		}
		
		eventC.shiftNewTitle(this);
	}
	
	public String getLatestTitle() {
		return titles.getLast();
	}

}
