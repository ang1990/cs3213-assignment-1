import java.util.LinkedList;

public class CircularShift {
	
	Alphabetizer eventA = new Alphabetizer();
	
	private LinkedList<String> circularShiftedTitles = new LinkedList<String>();
	
	public void reset() {
		circularShiftedTitles.clear();
		eventA.reset();
	}
	
	public void shiftNewTitle(Input eventI) {
		String[] words = eventI.getLatestTitle().split(" ");
		String titleToBeShifted = null;
		int length = 0;
		
		for(int i = 0; i < words.length; i++) {
			if(!eventI.isIgnoredWord(words[i])) {
				if(i == 0) {
					titleToBeShifted = words[i].substring(0, 1).toUpperCase().concat(words[i].substring(1));
				}
				else {
					titleToBeShifted = titleToBeShifted.concat(" " + 
										words[i].substring(0, 1).toUpperCase().concat(words[i].substring(1)));
				}
			}
			else {
				if(i == 0) {
					titleToBeShifted = words[i];
				}
				else {
					titleToBeShifted = titleToBeShifted.concat(" " + words[i]);
				}
			}
		}
		
		for(int i = 0; i < words.length; i++) {
			if(!eventI.isIgnoredWord(words[i])) {
				String titleToBeAdded = titleToBeShifted.substring(length) + " " + 
										titleToBeShifted.substring(0, length);
				addShiftedTitle(titleToBeAdded.trim());

			}
		
			length = length + words[i].length() + 1;
		}
		
		eventA.sortTitles();
	}
	
	public void addShiftedTitle(String title) {
		circularShiftedTitles.add(title);
		eventA.addNewTitle(this);
	}
	
	public String getLatestTitle() {
		return circularShiftedTitles.getLast();
	}
	
}
